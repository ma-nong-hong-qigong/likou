// 给你两个按 非递减顺序 排列的整数数组 nums1 和 nums2，另有两个整数 m 和 n ，分别表示 nums1 和 nums2 中的元素数目。

// 请你 合并 nums2 到 nums1 中，使合并后的数组同样按 非递减顺序 排列。

// 注意：最终，合并后数组不应由函数返回，而是存储在数组 nums1 中。为了应对这种情况，nums1 的初始长度为 m + n，其中前 m 个元素表示应合并的元素，后 n 个元素为 0 ，应忽略。nums2 的长度为 n 
// 示例 1：
// 输入：nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
// 输出：[1,2,2,3,5,6]
// 解释：需要合并 [1,2,3] 和 [2,5,6] 。
// 合并结果是 [1,2,2,3,5,6] ，其中斜体加粗标注的为 nums1 中的元素。
// 示例 2：

// 输入：nums1 = [1], m = 1, nums2 = [], n = 0
// 输出：[1]
// 解释：需要合并 [1] 和 [] 。
// 合并结果是 [1] 。
// 示例 3：

// 输入：nums1 = [0], m = 0, nums2 = [1], n = 1
// 输出：[1]
// 解释：需要合并的数组是 [] 和 [1] 。
// 合并结果是 [1] 。
// 注意，因为 m = 0 ，所以 nums1 中没有元素。nums1 中仅存的 0 仅仅是为了确保合并结果可以顺利存放到 nums1 中。
//现在就是要把 nums2 合并到 nums1 中并且还要排序
let merge = function (nums1, m, nums2, n) {
    //先把nums2插入 nums1 然后再排序
    //两个边界nums1位空和nums2为空时的处理
    // if (nums2.length === 0) {
    //     return
    // }
    // if (nums1.length = 0) {
    //     nums1 = [...nums2]
    //     return
    // }
    let n1 = 0
    let n2 = 0
    let arr = new Array(m + n).fill(0)
    let cur = 0
    while (n1 < m || n2 < n) {
        if (n1 === m) {
            //说明 nums1的数组已经取完了 要取 nums2这个数组
            cur = nums2[n2++]
        } else if (n2 === n) {
            //说明 nums2数组取完了 要去nums1数组
            cur = nums1[n1++]
        } else if (nums1[n1] < nums2[n2]) {

            cur = nums1[n1++]
        } else {
            cur = nums2[n2++]
        }
        arr[n1 + n2 - 1] = cur
    }
    for (let i = 0; i < m + n; i++) {
        nums1[i] = arr[i]

    }
    console.log(nums1);

};
let nums1 = [1, 2, 3, 0, 0, 0];
let m = 3;
let nums2 = [2, 5, 6];
let n = 3
merge(nums1, m, nums2, n)